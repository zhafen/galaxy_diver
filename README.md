# README #

### Repository purpose ###

This repository includes analysis tools for analyzing galaxy formation simulations, focusing on the FIRE simulations.
It's currently under development, with no official release yet.

### Setup ###

There is no automated setup yet. Currently you must clone the repository using
`git clone git@bitbucket.org:zhafen/galaxy_diver.git`
Afterwards you will need to add the repository to your python path.

### Contribution guidelines ###

I don't expect anyone to contribute to the repository at this point in time.

### Who do I talk to? ###

Zach Hafen (zachary.h.hafen@gmail.com) is the repository owner. Contact him for questions.
This was developed while working in [Northwestern University's galaxy formation group](http://galaxies.northwestern.edu/).
